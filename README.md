# 🐍 snek

## Usage

```bash
$ ./snek -h
```
```
usage: snek [-h] [--root ROOT] [--exe EXE] {envs,root,exe,info,fork,bless} ...

🐍

optional arguments:
  -h, --help            show this help message and exit
  --root ROOT           conda root folder / prefix (read from the current env, if not given)
  --exe EXE             conda or mamba executable (read from the current env, if not given)

commands:
  {envs,root,exe,info,fork,bless}
    envs                print existing envs
    root                print root
    exe                 print exe
    info                print info
    fork                fork a conda env
    bless               bless a conda env
```

